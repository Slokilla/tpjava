package clone;

import clone.TypeForme.FormeTete;
import clone.TypeForme.Taille;

public class Tete extends PartieDeMonstre{
	private FormeTete formeTete;
	private int nbYeux=0;
	private Taille tailleTete=Taille.Moyenne;
	
	public Tete() {formeTete =FormeTete.TeteHumaine;}

	public Tete(FormeTete formeTete, int nbYeux, Taille tailleTete) {
		super();
		this.formeTete = formeTete;
		this.nbYeux = nbYeux;
		this.tailleTete = tailleTete;
	}

	public FormeTete getForme() {
		return formeTete;
	}

	public void setForme(FormeTete forme) {
		this.formeTete = forme;
	}

	public int getNbYeux() {
		return nbYeux;
	}

	public void setNbYeux(int nbYeux) {
		this.nbYeux = nbYeux;
	}

	public Taille getTailleTete() {
		return tailleTete;
	}

	public void setTailleTete(Taille tailleTete) {
		this.tailleTete = tailleTete;
	}
}
