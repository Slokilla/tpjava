package clone;

import clone.TypeForme.FormePatte;
import clone.TypeForme.Taille;

public class Patte extends PartieDeMonstre{
	private FormePatte formePatte=FormePatte.Jambe;;
	private Taille taillePatte=Taille.Moyenne;
	
	public Patte() {}

	public Patte(FormePatte formePatte, Taille taillePatte) {
		super();
		this.formePatte = formePatte;
		this.taillePatte = taillePatte;
	}

	public FormePatte getForme() {
		return formePatte;
	}

	public void setForme(FormePatte forme) {
		this.formePatte = forme;
	}

	public Taille getTaillePatte() {
		return taillePatte;
	}

	public void setTaillePatte(Taille taillePatte) {
		this.taillePatte = taillePatte;
	}
}
