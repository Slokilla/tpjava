package Client;

import Composite.Constante;
import Composite.Contexte;
import Composite.ExpBool;
import Composite.Variable;
import Composite.Expou;
import Composite.Expet;
import Composite.Expnon;

public class Client {

	public static void main(String[] args) {
		final boolean vrai=true;
		final boolean faux=false;
		ExpBool expression;
		Contexte contexte=new Contexte();
		Variable x = new Variable("X"); // cr�e la variable X
		Variable y = new Variable("Y"); // cr�e la variable Y
		// construction de 1' expression bool�enne
		expression = new Expou( new Expet(x, (new Expnon(y))), new Expet(y, new Constante(vrai)));
		contexte.assigne(x, faux); // X vaut Faux
		contexte.assigne(y, vrai); // Y vaut Vrai
		// L'expression s'�value
		boolean resultat = expression.evalue(contexte);
		System.out.println("le resultat est : "+resultat);

	}

}
