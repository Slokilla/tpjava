package Composite;

import java.util.HashMap;

public class Contexte {
HashMap<Variable, Boolean> context;

public Contexte() {
	this.context = new HashMap<Variable, Boolean>();
}

public HashMap<Variable, Boolean> getContext() {
	return context;
}

public void setContext(HashMap<Variable, Boolean> context) {
	this.context = context;
}

public void assigne(Variable v, boolean b){
	context.put(v, b);
}
}