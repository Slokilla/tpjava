package Composite;

public class Variable extends ExpBool{
	
	String X;

	public Variable(String x) {
		super();
		X = x;
	}
	
	public String getNom(){
		return X;
	}
	
	public void setNom(String n){
		X = n;
	}
	@Override
	public boolean evalue(Contexte c) {
		return c.getContext().get(this);
	}

}

