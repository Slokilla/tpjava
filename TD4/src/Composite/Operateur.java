package Composite;


public abstract class Operateur extends ExpBool{

	protected ExpBool op1;
	protected ExpBool op2;
	
	public Operateur(ExpBool op1, ExpBool op2) {
		
		this.op1 = op1;
		this.op2 = op2;
	}
	public Operateur(ExpBool op1) {

		this.op1 = op1;
	}
	public Operateur() {
		super();
	}

}