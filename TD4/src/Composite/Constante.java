package Composite;

public class Constante extends ExpBool{

	
	boolean constante;
	
	public Constante(boolean constante) {
		super();
		this.constante = constante;
	}
	public boolean getBool(){
		return constante;
	}
	public void setBool(boolean b){
		constante = b;
	}
	@Override
	public boolean evalue(Contexte c) {
		return constante;
	}

}
