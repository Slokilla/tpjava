package Composite;

public class Expet extends Operateur {

    public Expet(ExpBool op1, ExpBool op2) {
        super(op1, op2);
    }

    @Override
    public boolean evalue(Contexte c) {
        return (op1.evalue(c) && op2.evalue(c));
    }


}
