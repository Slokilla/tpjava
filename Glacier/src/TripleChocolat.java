import java.util.ArrayList;

public class TripleChocolat extends CoupeGlacee {

    private static volatile TripleChocolat instance;

    private TripleChocolat() {
        super(new ArrayList<Parfum>() {{add(Parfum.CHOCOBLANC);
                  add(Parfum.CHOCOLAIT);
                  add(Parfum.CHOCONOIR);}},
                6.00f
        );
    }

    @Override
    public String getDescription() {
        String description = "Coupe ";
        for (Parfum parfum : parfumList) {
            description = description.concat(parfum.toString() + ' ');
        }
        return description;
    }

    @Override
    public float getPrix() {
        return prix;
    }

    public static TripleChocolat getInstance(){
        if (instance == null)
            instance = new TripleChocolat();
        return instance;
    }

}
