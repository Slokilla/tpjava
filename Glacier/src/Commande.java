import java.util.ArrayList;

public class Commande {

    private Clavier clavier = Clavier.getInstance();
    private ArrayList<Dessert> commande = new ArrayList<Dessert>();
    private float prix;

    public Commande() {
        super();
    }

    public void enregistrerCommande(){
        boolean fini = false;
        boolean correct;
        Dessert dessertActuel;

        while (!fini){
            dessertActuel = null;
            correct = true;
            System.out.println("choisissez une Coupe");
            System.out.println("1-Fruit Rouge");
            System.out.println("2-TripleChoco");
            System.out.println("0-Terminer la commande.");

            switch (clavier.lireInt()){
                case 1:
                    dessertActuel = FruitsRouges.getInstance();
                    break;
                case 2:
                    dessertActuel = TripleChocolat.getInstance();
                    break;
                case 0:
                    fini = true;
                    break;
                default:
                    System.out.println("Je n'ai pas compris");
                    correct = false;
                    break;
            }
            if(correct && !fini){

                boolean finTopping = false;

                while (!finTopping) {
                    System.out.println("Voulez-vous ajouter un Topping ?");
                    System.out.println("1-Chantilly");
                    System.out.println("2-Sauce Choco");
                    System.out.println("3-Coulis Fraise");
                    System.out.println("0-Terminer la Commande");

                    switch (clavier.lireInt()) {
                        case 1:
                            dessertActuel = new Chantilly(dessertActuel);
                            break;
                        case 2:
                            dessertActuel = new SauceChoco(dessertActuel);
                            break;
                        case 3:
                            dessertActuel = new Coulis(dessertActuel);
                            break;
                        case 0:
                            finTopping = true;
                            break;
                        default:
                            System.out.println("Je n'ai pas compris");
                            break;
                    }
                }
            }
            if (dessertActuel != null){
                addDessert(dessertActuel);
            }
        }
    }

    public void editerFacture(){
        for (Dessert dessert : commande){
            System.out.print(dessert.getDescription());
            System.out.println(" + " + dessert.getPrix());
        }
        System.out.println("        TOTAL : " + Float.toString(prix));
    }

    private void addDessert(Dessert dessert){
        commande.add(dessert);
        prix += dessert.getPrix();
    }

}
