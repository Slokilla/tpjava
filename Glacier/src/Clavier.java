import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;


public class Clavier {
	private static Clavier instance;
	private BufferedReader bufIn;
	private StringTokenizer st=null;

	public static synchronized Clavier getInstance(){
		if (instance == null)
			instance = new Clavier();
		return instance;
	}

	private Clavier()
	{
		bufIn=new BufferedReader(new InputStreamReader(System.in));
	}
	
	public synchronized void read()
	{
		try
		{
			String s=bufIn.readLine();
			st=new StringTokenizer(s);
		}
		catch (IOException e){
			System.out.println("erreur read "+e.getMessage());
			System.exit(2);
		}
	}
	
	private synchronized void flushTotal() {
		st=null;
		bufIn=null;
	}
	
	private synchronized void flush()
	{
		st=null;
	}
    public synchronized int lireInt()  {
    	if (st == null)
    	    read();
    	while (! st.hasMoreTokens())
    	    read();
    	String ss = st.nextToken();
		return(Integer.parseInt(ss));
        }

        public synchronized long lireLong()  {
    	if (st == null)
    	    read();
    	while (! st.hasMoreTokens())
    	    read();
    	String ss = st.nextToken();
			return(Long.parseLong(ss));
        }

        public synchronized float lireFloat()  {
    	if (st == null)
    	    read();
    	while (! st.hasMoreTokens())
    	    read();
    	String ss = st.nextToken();
			return(Float.parseFloat(ss));
        }
        
        public synchronized double lireDouble()  {
    	if (st == null)
    	    read();
    	while (! st.hasMoreTokens())
    	    read();
    	String ss = st.nextToken();
			return(Double.parseDouble(ss));
        }

        public synchronized String lireString()  {
    	if (st == null)
    	    read();
    	while (! st.hasMoreTokens())
    	    read();
    	return(st.nextToken());
        }

        public synchronized String lireLigne()  {
    	String s = "" ;
    	if ((st == null) || (!st.hasMoreTokens()))
    	{
    	    try{
    		s = bufIn.readLine() ;
    	    } catch (IOException e) {
    		System.out.println("lireString" + " " + e.getMessage());
    		System.exit(2); // Une erreur s'est produite, on sort du programme.
    	    }
    	    return s ;
    	}
    	else
    	{
    	    System.out.println("Autre cas") ;
    	    return(st.nextToken(System.getProperty("line.separator")));
    	}
        }
}
