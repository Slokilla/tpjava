public class SauceChoco extends Topping {

    public SauceChoco(Dessert dessert) {
        super(dessert);
        this.surcout = 0.7f;
        this.description = " et sa délicieuse sauce chocolat";
    }

    @Override
    public String getDescription() {
        String description = dessert.getDescription();
        description = description.concat("\n " + this.description);

        return description;
    }

    @Override
    public float getPrix() {
        return dessert.getPrix() + this.surcout;
    }


}
