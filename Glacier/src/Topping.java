import java.util.ArrayList;

public abstract class Topping implements Dessert{

    protected Dessert dessert;
    protected String description;
    protected float surcout;

    protected Topping() {
        super();
    }

    protected Topping(Dessert dessert){
        this.dessert = dessert;
    }

}
