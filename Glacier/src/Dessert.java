public interface Dessert {

    float getPrix();
    String getDescription();

}
