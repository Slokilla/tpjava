import java.util.ArrayList;

public abstract class CoupeGlacee implements Dessert {

    private CoupeGlacee instance = null;
    protected float prix;
    protected ArrayList<Parfum> parfumList = new ArrayList<Parfum>();


    public CoupeGlacee() {
    }

    protected CoupeGlacee(ArrayList<Parfum> parfums, float prix){
        this.parfumList = parfums;
        this.prix = prix;
    }

    public abstract String getDescription();
    public abstract float getPrix();



}
