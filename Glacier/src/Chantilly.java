public class Chantilly extends Topping {

    Chantilly(Dessert dessert){
        super(dessert);
        this.surcout = 0.50f;
        this.description = " avec de la Chantilly";
    }

    @Override
    public String getDescription() {
        String description = dessert.getDescription();
        description = description.concat("\n " + this.description);

        return description;
    }

    @Override
    public float getPrix() {
        return dessert.getPrix() + this.surcout;
    }
}
