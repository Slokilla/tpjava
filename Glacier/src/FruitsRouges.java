import java.awt.*;
        import java.util.ArrayList;

public class FruitsRouges extends CoupeGlacee {

    private static volatile FruitsRouges instance = null;

    private FruitsRouges() {
        super(new ArrayList<Parfum>() {{add(Parfum.FRAISE);
                  add(Parfum.FRAMBOISE);
                  add(Parfum.CASSIS);}},
                5.50f
        );
    }

    @Override
    public String getDescription() {
        String description = "Coupe ";
        for (Parfum parfum : parfumList) {
            description = description.concat(parfum.toString() + ' ');
        }
        return description;
    }

    @Override
    public float getPrix() {
        return prix;
    }


    public static FruitsRouges getInstance() {
        if(instance == null)
            instance = new FruitsRouges();
        return instance;
    }
}
