public class Coulis extends Topping{

    Coulis(Dessert dessert){
        super(dessert);
        this.surcout = 1.0f;
        this.description = " au coulis de fraises fraîches";
    }

    @Override
    public String getDescription() {
        String description = dessert.getDescription();
        description = description.concat("\n " + this.description);

        return description;
    }

    @Override
    public float getPrix() {
        return dessert.getPrix() + this.surcout;
    }

}
