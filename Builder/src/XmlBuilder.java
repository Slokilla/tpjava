import data.Contenu;
import data.Data;
import data.Titre;

import java.util.LinkedList;

public class XmlBuilder extends TypeBuilder {


    @Override
    public final void setData(final LinkedList<Data> d) {
        this.data = d;
    }

    @Override
    public final void buildTitre(final Titre t) {
        document.addContenu(
                "<XmlBaliseTitre>" + t.lire() + "</XmlBaliseTitre>\n");
    }

    @Override
    public final void buildContenu(final Contenu c) {
        document.addContenu(
                "<xmlBaliseContenu>" + c.lire() + "</xmlBaliseContenu>\n");
    }

}
