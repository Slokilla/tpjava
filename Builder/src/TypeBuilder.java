import data.Contenu;
import data.Data;
import data.Section;
import data.Titre;

import java.util.LinkedList;

public abstract class TypeBuilder implements DocumentBuilder {

    /**
     * Le document qu'on va remplir.
     */
    protected Document document = new Document();

    /**
     * Les données à utiliser.
     */
    protected LinkedList<Data> data;

    @Override
    public abstract void setData(LinkedList<Data> d);

    @Override
    public abstract void buildTitre(Titre t);

    @Override
    public abstract void buildContenu(Contenu c);

    /**
     * Méthode qui réalise le parcours en profondeur de données.
     * @param datas les données à parcourir
     * @throws Exception si un type de données est inconnu
     */
    public void getData(final LinkedList<Data> datas) throws Exception {
        for (Data d : datas) {
            if (d.getClass() == Titre.class) {
                buildTitre((Titre) d);
            } else if (d.getClass() == Contenu.class) {
                buildContenu((Contenu) d);
            } else if (d.getClass() == Section.class) {
                getData(((Section) d).getSections());
            } else {
                throw new Exception("Le type utilisé est inconnu");
            }
        }
    }

    @Override
    public final Document getDocument() throws Exception {
        getData(data);
        return document;
    }
}
