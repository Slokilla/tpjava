import data.Data;

import java.util.LinkedList;

public class Document {

    /**
     * La liste de composant du Document.
     */
    private StringBuilder contenu = new StringBuilder();

    /**
     * Le constructeur de document par défaut.
     */
    public Document(){

    }

    /**
     * Methode qui ajoute du texte dans le document.
     * @param s le texte à entrer dans le document
     */
    public void addContenu(final String s) {
        this.contenu.append(s);
    }

    /**
     * Methode qui lance le parcours en profondeur des données.
     */
    public void afficher() {
        System.out.println(contenu);
    }
}
