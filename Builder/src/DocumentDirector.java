import data.Data;

import java.util.LinkedList;

public class DocumentDirector {

    /**
     * Le builder à utliser.
     */
    private final DocumentBuilder builder;

    //private LinkedList<Data> data = new LinkedList<Data>();

    /**
     * Les données à exploiter.
     */
    private Data data = null;

    /**
     * Constructeur qui instancie le Directeur avec un builder.
     * @param b Le builder à utiliser
     */
    public DocumentDirector(final DocumentBuilder b) {
        this.builder = b;
    }

    /**
     * La méthode construit le document.
     * @param d les données avec lesquelles construire le document
     */
    public void construct(final LinkedList<Data> d) {
        builder.setData(d);
    }

/*    public void setData(LinkedList<Data> d){
        this.data = d;
    }*/

    /**
     * Setter des données à utiliser.
     * @param d les donnée à utiliser
     */
    public void setData(final Data d) {
        this.data = d;
    }

    /**
     * Méthode qui lance la construction du document.
     * @return le document buildé
     * @throws Exception si le type d'un donnée est inconnu
     */
    public Document getDocument() throws Exception {
        construct(new LinkedList<>() {{
            add(data);
        }});
        return builder.getDocument();
    }

}
