import data.Contenu;
import data.Section;
import data.Titre;

public class Main {
    public static void main(String[] args) {

        //DocumentBuilder builder = new XmlBuilder();
        DocumentBuilder builder = new DocxBuilder();
        DocumentDirector dir = new DocumentDirector(builder);

        Titre titre = new Titre("Titre");

        Contenu contenu1 = new Contenu("Lorem Ipsum Dolor Sit Amet");
        Contenu contenu2 = new Contenu("Lorem Ipsum Dolor Sit Amet Et tout et tout");
        Contenu contenu3 = new Contenu("Lorem Ipsum Dolor Sit Amet 3");

        Section section = new Section();
        Section section2 = new Section();

        section.addData(titre);
        section.addData(contenu1);
        section2.addData(contenu2);
        section2.addData(contenu3);

        section.addData(section2);

        dir.setData(section);
        Document doc = null;
        try {
            doc = dir.getDocument();
            doc.afficher();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
