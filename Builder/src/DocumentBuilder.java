import data.Contenu;
import data.Data;
import data.Titre;

import java.util.LinkedList;

public interface DocumentBuilder {

    /**
     * Setter des données du builder.
     * @param d les données que le builder utilisera
     */
    void setData(LinkedList<Data> d);

    /**
     * Le builder de titre.
     * @param t l'objet à "titrer"
     */
    void buildTitre(Titre t);

    /**
     * Le builder de contenu.
     * @param c l'objet à mettre en forme
     */
    void buildContenu(Contenu c);

    /**
     * la methode qui effectue le build en profondeur.
     * @param datas les données à parcourir
     * @throws Exception si un des types de données est inconnu
     */
    void getData(LinkedList<Data> datas) throws Exception;

    /**
     * Méthode qui lance le build en profondeur et renvoie le
     * document.
     * @return Le document mis en forme
     * @throws Exception si un des types de données est inconnu
     */
    Document getDocument() throws Exception;

}
