package data;

public class Titre extends Texte {

    /**
     * Constructeur hérité.
     * @param t le texte du titre
     */
    public Titre(final String t) {
        super(t);
    }

}
