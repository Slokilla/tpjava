package data;

import java.util.LinkedList;

public class Section extends Data {
    /**
     * La liste des composant de la section.
     */
    private LinkedList<Data> sections = new LinkedList<Data>();

    @Override
    public final String lire() {
        for (Data d : sections) {
            d.lire();
        }
        return null;
    }

    /**
     * Getter de la liste de composants de Section.
     * @return sections
     */
    public final LinkedList<Data> getSections() {
        return sections;
    }

    /**
     * Ajoute des composant à la liste des composants.
     * @param d la donnée à ajouter
     */
    public final void addData(final Data d) {
        sections.add(d);
    }
}
