package data;

public class Contenu extends Texte {

    /**
     * Constructeur hérité.
     * @param t le texte
     */
    public Contenu(final String t) {
        super(t);
    }

}
