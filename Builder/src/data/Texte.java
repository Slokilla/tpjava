package data;

public abstract class Texte extends Data {

    /**
     * Le texte contenu dans le composant.
     */
    private String texte = "";

    /**
     * Le constructeur qui instance le composant avec un texte.
     * @param t le texte du composant
     */
    public Texte(final String t) {
        this.texte = t;
    }

    @Override
    public final String lire() {
        return texte;
    }

    /**
     * Setter du texte du composant.
     * @param t le texte de remplacement
     */
    public final void setTexte(final String t) {
        this.texte = t;
    }
}
