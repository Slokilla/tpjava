package data;

public abstract class Data {

    /**
     * Methode qui lira la donnée.
     */
    public abstract String lire();
}
