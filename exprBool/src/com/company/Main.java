package com.company;

public class Main {

    public static void main(String[] args) {

        ExpBool expression;
        Contexte contexte=new Contexte();
        Variable x = new Variable("X"); // crée la variable X
        Variable y = new Variable("Y"); // crée la variable Y
        // construction de 1' expression booléenne
        expression = new ExpOU( new ExpET(x, (new ExpNON(y))), new ExpET(y, new
                Constante(true)));
        contexte.assigne(x, false); // X vaut Faux
        contexte.assigne(y, true); // Y vaut Vrai
        // L'expression s'évalue
        boolean resultat = expression.Evalue(contexte);

        System.out.println(Boolean.toString(resultat));

    }
}
