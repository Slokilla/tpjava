package com.company;

public class ExpNON extends Operateur {
    public ExpNON(ExpBool op1) {
        super(op1);
    }

    @Override
    public boolean Evalue(Contexte c) {
        return !op1.Evalue(c);
    }
}
