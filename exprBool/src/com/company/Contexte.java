package com.company;

import java.util.HashMap;

public class Contexte {

    private HashMap<Variable,Boolean> contexte;

    public HashMap<Variable, Boolean> getContexte() {
        return contexte;
    }

    public void setContexte(HashMap<Variable, Boolean> contexte) {
        this.contexte = contexte;
    }

    public Contexte(){
        super();
        contexte = new HashMap<Variable,Boolean>();
    }

    public void assigne(Variable var, Boolean val){
        contexte.put(var,val);
    }
}
