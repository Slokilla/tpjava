package com.company;

public class ExpET extends Operateur {

    public ExpET(ExpBool op1, ExpBool op2) {
        super(op1, op2);
    }

    @Override
    public boolean Evalue(Contexte c) {
        return (op1.Evalue(c) && op2.Evalue(c));
    }
}
