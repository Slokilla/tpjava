package com.company;

public class Variable extends ExpBool{
    private String nom;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Variable(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean Evalue(Contexte contexte) {
        return contexte.getContexte().get(this);
    }
}
