package com.company;

public class ExpOU extends Operateur {

    public ExpOU(ExpBool op1, ExpBool op2) {
        super(op1, op2);
    }

    @Override
    public boolean Evalue(Contexte c) {
        return op1.Evalue(c) || op2.Evalue(c);
    }
}
