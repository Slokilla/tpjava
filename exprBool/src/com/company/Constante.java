package com.company;

public class Constante extends ExpBool {
    private boolean Valeur;

    public boolean isValeur() {
        return Valeur;
    }

    public void setValeur(boolean valeur) {
        Valeur = valeur;
    }

    public Constante(boolean valeur) {
        super();
        Valeur = valeur;
    }

    @Override
    public boolean Evalue(Contexte contexte) {
        return Valeur;
    }
}
