package modele;

import java.util.LinkedList;

public class Compte {
	private int cle;
	private String num_compte;
	private float solde;
	private LinkedList<Operation> lesOperations;
	private Banque banque;
	
	public Compte() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	public Compte(String num_compte, float solde) {
		super();
		this.num_compte = num_compte;
		this.solde = solde;
	}
	
	public Compte(int id, String num_compte, float solde) {
		super();
		this.cle = id;
		this.num_compte = num_compte;
		this.solde = solde;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Compte other = (Compte) obj;
		if (cle != other.cle)
			return false;
		if (num_compte != other.num_compte)
			return false;
		if (Float.floatToIntBits(solde) != Float.floatToIntBits(other.solde))
			return false;
		return true;
	}


	public int getCle() {
		return cle;
	}


	public void setCle(int cle) {
		this.cle = cle;
	}


	public String getNum_compte() {
		return num_compte;
	}


	public void setNum_compte(String num_compte) {
		this.num_compte = num_compte;
	}


	public float getSolde() {
		return solde;
	}


	public void setSolde(float solde) {
		this.solde = solde;
	}


	public LinkedList<Operation> getLesOperations() {
		return lesOperations;
	}


	public void setLesOperations(LinkedList<Operation> lesOperations) {
		this.lesOperations = lesOperations;
	}
	
	
	public Operation addOperation(String intitule, float montant, String date ) {
		if (lesOperations==null) 
			lesOperations=new LinkedList<Operation>();
		lesOperations.add(new Operation(intitule,montant, date, this.getCle()));
		return null;
		
	}
	
	
}
