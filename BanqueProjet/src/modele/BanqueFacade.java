package modele;

import dao.CompteDAOMySQL;
import dao.DAOGenerique;
import dao.OperationDAOMySQL;
import daoFactory.DAOFactory;
import daoFactory.DAOJsonFactory;
import daoFactory.DAOMySqlFactory;

import java.util.LinkedList;

public class BanqueFacade {

    private static BanqueFacade instance = null;

    public DAOGenerique<Compte> compteManager = null;
    public DAOGenerique<Operation> operationManager = null;

    public LinkedList<Compte> getLesComptes(){
        return compteManager.findAll();
    }

    private BanqueFacade(){
        DAOFactory factory = DAOFactory.getFactory(DAOFactory.sourceDonnee.MySQL);
        compteManager = factory.getCompteDAO();
        operationManager = factory.getOperationDAO();
    }


    public Compte findCompte(int cle){
        return compteManager.findById(cle);
    }

    public Compte ajouterCompte(String numCompte, float solde){
        return compteManager.create(new Compte(numCompte,solde));
    }

    public Compte enregistrerCompte(int cleCompte){
        Banque banque = new Banque();
        return new Compte();
    }

    public Compte ajouterOperation(String intitule, String date, float montant, Compte c){
        Operation ope = operationManager.create(new Operation(intitule,montant,date,c.getCle()));
        c.addOperation(intitule,montant,date);
        return c;
    }

    public static BanqueFacade getFacade(){
        if(instance == null){
            instance = new BanqueFacade();
        }
        return instance;
    }

    public void saveAll(){

    }
}
