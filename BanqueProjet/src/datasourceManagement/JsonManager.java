package datasourceManagement;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import modele.Compte;

import java.io.*;
import java.lang.reflect.Type;
import java.util.LinkedList;

public class JsonManager {

    private static volatile JsonManager instance = null;
    private final String path = "D:\\PROJETS\\tpjava\\BanqueProjet\\banque.json";
    final GsonBuilder builder = new GsonBuilder();
    final Gson gson = builder.create();
    String json = "";

    private JsonManager() {
        super();
    }

    public static JsonManager getInstance(){
        if(instance == null)
            instance = new JsonManager();
        return instance;
    }

    public LinkedList<Compte> getData() {
        /*
         * Etape 1
         * Lecture du fichier json ligne � ligne et remplissage de la variable json
         */
        try {
            InputStream ips = new FileInputStream(path);
            InputStreamReader ipsr = new InputStreamReader(ips);
            BufferedReader br = new BufferedReader(ipsr);
            String ligne;
            while ((ligne = br.readLine()) != null) {
                System.out.println(ligne);
                json += ligne;
            }
            br.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

        if (json.compareTo("") == 0) {
            return new LinkedList<Compte>();
        } else {
            Type type = new TypeToken<LinkedList<Compte>>() {
            }.getType();
            return gson.fromJson(json, type);
        }
    }

    public int setData(LinkedList<Compte> comptesToSave) {
        json = gson.toJson(comptesToSave);
        File fichier = new File(path);
        Writer writer = null;
        try {
            // On écrit juste ce qui est généré par gson
            writer = new FileWriter(fichier);
            writer.write(json);
        } catch (IOException e) {
            System.out.println("écriture impossible " + e.getMessage());
            e.printStackTrace();
        } finally {
            if (writer != null) {
                try {
                    writer.close();
                } catch (IOException e) {
                    System.out.println("Erreur de fermeture du le fichier : " + e.getMessage());
                    e.printStackTrace();
                }
            }
        }
        return 0;
    }


}
