package daoFactory;

import dao.*;
import modele.Compte;
import modele.Operation;

public class DAOJsonFactory extends DAOFactory {
    private static DAOFactory instance = null;

    private DAOJsonFactory(){
        super();
    }

    @Override
    public DAOGenerique<Compte> getCompteDAO(){
        return CompteDAOJson.getInstance();
    }

    @Override
    public DAOGenerique<Operation> getOperationDAO(){
        return OperationDAOJson.getInstance();
    }

    public static DAOFactory getJsonFactory() {
        if(instance == null){
            instance = new DAOJsonFactory();
        }
        return instance;
    }

}
