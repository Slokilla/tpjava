package daoFactory;

import dao.CompteDAOMySQL;
import dao.DAOGenerique;
import dao.OperationDAOMySQL;
import modele.Compte;
import modele.Operation;

public class DAOMySqlFactory extends DAOFactory {
    private static DAOFactory instance = null;

    private DAOMySqlFactory(){
       super();
    }

    @Override
    public DAOGenerique<Compte> getCompteDAO(){
        return CompteDAOMySQL.getCompteManager();
    }

    @Override
    public DAOGenerique<Operation> getOperationDAO(){
        return OperationDAOMySQL.getOperationManager();
    }

    public static DAOFactory getMySQLFactory() {
        if(instance == null){
            instance = new DAOMySqlFactory();
        }
        return instance;
    }

}
