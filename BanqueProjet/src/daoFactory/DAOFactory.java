package daoFactory;

import dao.DAOGenerique;
import datasourceManagement.MySQLManager;
import modele.Compte;
import modele.Operation;

abstract public class DAOFactory {

    public static enum sourceDonnee{
        MySQL,
        json
    }

    abstract public DAOGenerique<Compte> getCompteDAO();
    abstract public DAOGenerique<Operation> getOperationDAO();

    public static DAOFactory getFactory(sourceDonnee sd){
        switch (sd){
            case json:
                return DAOJsonFactory.getJsonFactory();
            case MySQL:
                return DAOMySqlFactory.getMySQLFactory();
            default:
                throw new IllegalArgumentException();
        }
    }

}
