import dao.CompteDAOMySQL;
import dao.DAOGenerique;
import dao.OperationDAOMySQL;
import daoFactory.DAOFactory;
import daoFactory.DAOMySqlFactory;
import modele.Banque;
import modele.Compte;
import modele.Operation;
import view.BanqueView;

import java.lang.reflect.Array;

public class main {

	public static void main(String[] args) {
		/**
		 * Test partie DAO
		 */
		/*//CompteDAOMySQL compteManager = CompteDAOMySQL.getManager();
		DAOGenerique<Compte> compteManager = CompteDAOMySQL.getSqlManager();
		OperationDAOMySQL opeManager = OperationDAOMySQL.getManager();
		
		Compte c = new Compte();
		c.setNum_compte("B2dfqdf8");
		c.setSolde((float) 13.31);

		c = compteManager.create(c);
		
		Operation o = new Operation("caca", (float) 18, "18/07", c.getCle());

		opeManager.create(o);
		System.out.println("fini");*/

		/**
		 * Test partie Factory
		 */
/*
		DAOGenerique<Compte> comptes = DAOFactory.getFactory(DAOFactory.sourceDonnee.MySQL).getCompteDAO();
		DAOGenerique<Operation> operations = DAOFactory.getFactory(DAOFactory.sourceDonnee.MySQL).getOperationDAO();

		System.out.println(comptes.findById(1).getSolde());
		System.out.println(operations.findById(2).getIntitule());*/

		BanqueView.main(args);

	}

}
