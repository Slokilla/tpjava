package dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import datasourceManagement.MySQLManager;
import modele.Compte;
import modele.Operation;

public class CompteDAOMySQL extends DAOGenerique<Compte> {

	/**
	 * Gestion du singleton
	 */
	private static CompteDAOMySQL instance =null ;
	private static MySQLManager sqlManager =null ;
	private static OperationDAOMySQL opeManager =null ;
	public static CompteDAOMySQL getCompteManager() {

		if(instance == null) {
			sqlManager = MySQLManager.getInstance();
			opeManager = OperationDAOMySQL.getOperationManager();
			instance = new CompteDAOMySQL();
		}

		return instance;
	}

	/**
	 * Constructor.
	 */
	private CompteDAOMySQL() {
		super();
	}

	/**
	 * Creaction de compte
	 * @param c un objet compte
	 * @return le compte créé
	 */
	@Override
	public Compte create(Compte c) {
		String req = "INSERT INTO Compte (num_compte, solde) VALUES('" + c.getNum_compte() +"','" + c.getSolde() +"');";
		int id = sqlManager.setData(req);

		c.setCle(id);
		
		return c;
	}

	/**
	 * Unused
	 * @param c un compte
	 */
	@Override
	public Compte delete(Compte c) {
		return new Compte();
	}

	/**
	 * Mise à jour de compte.
	 * @param c un objet compte recensant les modifs
	 * @return un compte modifié
	 */
	@Override
	public Compte update(Compte c) {
		String req = "UPDATE Compte SET num_compte = '"+ c.getNum_compte() +"', solde = '" + c.getSolde() +";";
		int id = sqlManager.setData(req);
		
		return c;
	}

	/**
	 * Enregistre une liste de compte en base.
	 * @param lesComptes une liste de comptes
	 */
	@Override
	public void saveAll(LinkedList<Compte> lesComptes) {
		for(Compte c : lesComptes) {
			update(c);
		}
	}

	/**
	 * Retourne le compte correspondant à l'id en param.
	 * @param cle la clé du compte qu'on cherche
	 * @return le compte
	 */
	@Override
	public Compte findById(int cle){
		Compte cpt = new Compte();
		String req = "SELECT * FROM compte WHERE compte.id = "+ cle +";";
		
		ResultSet res = sqlManager.getData(req);
		
		try {
			if(res.first()) {
				int id = res.getInt("id");
				String num_compte = res.getString("num_compte");
				float solde = res.getFloat("solde");
				cpt.setCle(id);
				cpt.setNum_compte(num_compte);
				cpt.setSolde(solde);
				opeManager.findByName(Integer.toString(cpt.getCle()));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return cpt;
	}

	@Override
	public LinkedList<Compte> findAll(){
		LinkedList<Compte> listCpt = new LinkedList<Compte>();
		
		String req = "SELECT * FROM compte";
		
		ResultSet res = sqlManager.getData(req);
		
		try {
			while(res.next()) {
				Compte cpt = new Compte();
				int id = res.getInt("id");
				String num_compte = res.getString("num_compte");
				float solde = res.getFloat("solde");
				cpt.setCle(id);
				cpt.setNum_compte(num_compte);
				cpt.setSolde(solde);
				cpt.setLesOperations(opeManager.findByName(Integer.toString(id)));
				
				listCpt.add(cpt);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listCpt;
	}

	@Override
	public LinkedList<Compte> findByName(String name ){
		return null;
	}
	
}
