package dao;
import datasourceManagement.MySQLManager;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;

import modele.Compte;
import modele.Operation;

public class OperationDAOMySQL extends DAOGenerique<Operation>{

	/**
	 * Gestion du singleton
	 */
	private static volatile OperationDAOMySQL instance = null;
	private static volatile MySQLManager sqlManager = null;
	public static OperationDAOMySQL getOperationManager() {
		if(instance == null)
			sqlManager = MySQLManager.getInstance();
			instance = new OperationDAOMySQL();
		return instance;
	}

	/**
	 * Constructor.
	 */
	private OperationDAOMySQL() {
		super();
	}

	/**
	 * Créateur d'opération
	 * @param o un objet opération
	 * @return une opération en base
	 */
	@Override
	public Operation create(Operation o) {
		String req = "INSERT INTO Operation (intitule, montant, dateOperation, id_compte) VALUES ('" + o.getIntitule() +"'," + o.getMontant() +",'" + o.getDateOperation()  +"', " + o.getCleCompte() +");";
		System.out.println(req);
		int id = sqlManager.setData(req);
	
		o.setCle(id);
		
		return o;
	}

	/**
	 * Unused
	 * @param o un objet opération
	 */
	@Override
	public Operation delete(Operation o) {
		return new Operation();
	}

	/**
	 * Mise à jour d'une opération
	 * @param o un objet opération accusant des changements à effectuer
	 * @return une operation
	 */
	@Override
	public Operation update(Operation o) {
		String req = "UPDATE Operation SET intitule = '"+ o.getIntitule() +"', montant = '" + o.getMontant() + "', date = '" + o.getDateOperation() + "', id_compte = '" + o.getCleCompte() + "';";
		int id = sqlManager.setData(req);
		
		return o;
	}

	/**
	 * Enregistre une liste d'opérations en base
	 * @param lesOperations une liste d'objets operation
	 */
	@Override
	public void saveAll(LinkedList<Operation> lesOperations) {
		for(Operation o : lesOperations) {
			update(o);
		}
	}

	/**
	 * Trouve l'operation correspandant à une clé
	 * @param cle l'id d'une operation
	 * @return l'operation correspondant à l'id
	 */
	@Override
	public Operation findById(int cle){
		Operation ope = new Operation();
		String req = "SELECT * FROM operation WHERE operation.cle = "+ cle +";";

		ResultSet res = sqlManager.getData(req);

		try {
			if(res.first()) {
				int id = res.getInt("cle");
				String intitule = res.getString("intitule");
				float montant = res.getFloat("montant");
				String dateOperation = res.getString("dateOperation");
				int id_compte = res.getInt("id_compte");
				ope.setCle(id);
				ope.setIntitule(intitule);
				ope.setMontant(montant);
				ope.setCleCompte(id_compte);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return ope;
	}

	/**
	 * Trouve toutes les opérations
	 * @return une liste d'opérations
	 */
	@Override
	public LinkedList<Operation> findAll(){
		LinkedList<Operation> listOpe = new LinkedList<Operation>();
		
		String req = "SELECT * FROM operation";
		
		ResultSet res = sqlManager.getData(req);
		
		try {
			while(res.next()) {
				Operation ope = new Operation();
				int cle = res.getInt("cle");
				String intitule = res.getString("intitule");
				Float montant = res.getFloat("montant");
				String dateOperation = res.getString("dateOperation");
				int cleCompte = res.getInt("id_compte");
				ope.setCle(cle);
				ope.setCleCompte(cleCompte);
				ope.setDateOperation(dateOperation);
				ope.setIntitule(intitule);
				ope.setMontant(montant);
				
				listOpe.add(ope);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listOpe;
	}

	/**
	 * Trouve toutes les opérations correspondant à un compte
	 * @param name l'id du compte (string à cause de l'abstraction sous-jacente)
	 * @return une liste de comptes
	 */
	@Override
	public LinkedList<Operation> findByName(String name ){
		LinkedList<Operation> listOpe = new LinkedList<Operation>();
		
		String req = "SELECT * FROM operation WHERE id_compte = " + Integer.parseInt(name) + ";";
		
		ResultSet res = sqlManager.getData(req);
		
		try {
			while(res.next()) {
				Operation ope = new Operation();
				int cle = res.getInt("cle");
				String intitule = res.getString("intitule");
				float montant = res.getFloat("montant");
				String dateOperation = res.getString("dateOperation");
				int cleCompte = res.getInt("id_compte");
				ope.setCle(cle);
				ope.setCleCompte(cleCompte);
				ope.setDateOperation(dateOperation);
				ope.setIntitule(intitule);
				ope.setMontant(montant);
				
				listOpe.add(ope);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return listOpe;
	}
}
