package dao;

import datasourceManagement.MySQLManager;

import java.util.LinkedList;

public abstract class DAOGenerique<T> {


    abstract public T create(T obj);
    abstract public T delete(T obj);
    abstract public T update(T obj);
    abstract public void saveAll(LinkedList<T> toSave);
    abstract public T findById(int cle);
    abstract public LinkedList<T> findByName(String name);
    abstract public LinkedList<T> findAll();
}
