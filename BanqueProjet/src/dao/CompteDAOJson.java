package dao;

import datasourceManagement.JsonManager;
import modele.Compte;

import java.util.LinkedList;

public class CompteDAOJson extends DAOGenerique<Compte>{

    private static volatile DAOGenerique<Compte> instance = null;
    private static volatile JsonManager jsonManager = null;

    private CompteDAOJson() {
        super();
        jsonManager = JsonManager.getInstance();
    }

    public static DAOGenerique<Compte> getInstance() {
        if (instance == null)
            instance = new CompteDAOJson();
        return instance;
    }

    @Override
    public Compte create(Compte c) {
        //On récupère tout les compte, et on ajoute le nouveau à la fin
        LinkedList<Compte> comptes = jsonManager.getData();
        int MaxID = 0;
        for (Compte compteSaved : comptes) {
            MaxID = Math.max(MaxID, compteSaved.getCle());
        }
        c.setCle(MaxID + 1);
        comptes.add(c);
        jsonManager.setData(comptes);

        return c;
    }

    @Override
    public Compte delete(Compte obj) {
        return null;
    }

    @Override
    public Compte update(Compte c) {
        int IDToDelete = c.getCle();
        int indexOfCompte = 0;
        LinkedList<Compte> lesComptes = jsonManager.getData();
        for (Compte compte : lesComptes) {
            if (compte.getCle() == IDToDelete) {
                indexOfCompte = lesComptes.indexOf(compte);
            }
        }
        lesComptes.remove(indexOfCompte);
        lesComptes.add(c);
        jsonManager.setData(lesComptes);
        return c;
    }

    @Override
    public void saveAll(LinkedList<Compte> toSave) {
        LinkedList<Compte> comptes = jsonManager.getData();
        int MaxID = 0;
        for (Compte compteSaved : comptes) {
            MaxID = Math.max(MaxID, compteSaved.getCle());
        }
        for (Compte compteToSave : toSave){
            compteToSave.setCle(MaxID + 1);
            comptes.add(compteToSave);
        }
        jsonManager.setData(comptes);
    }

    @Override
    public Compte findById(int cle) {
        Compte compte = null;
        LinkedList<Compte> lesComptes = jsonManager.getData();
        for (Compte c : lesComptes) {
            if (c.getCle() == cle) {
                return c;
            }
        }
        return null;
    }

    @Override
    public LinkedList<Compte> findByName(String name) {
        return null;
    }

    @Override
    public LinkedList<Compte> findAll() {
        return jsonManager.getData();
    }
}
