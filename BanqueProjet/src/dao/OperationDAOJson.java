package dao;

import datasourceManagement.JsonManager;
import modele.Compte;
import modele.Operation;

import java.util.LinkedList;

public class OperationDAOJson extends DAOGenerique<Operation> {

    private static volatile DAOGenerique<Operation> instance = null;
    private static volatile JsonManager jsonManager = null;

    private OperationDAOJson() {
        super();
        jsonManager = JsonManager.getInstance();
    }

    public static DAOGenerique<Operation> getInstance() {
        if (instance == null)
            instance = new OperationDAOJson();
        return instance;
    }


    @Override
    public Operation create(Operation o) {
        LinkedList<Compte> lesComptes = jsonManager.getData();
        int CleMax = 0;
        for (Compte compte : lesComptes) {
            for (Operation operation : compte.getLesOperations()) {
                CleMax = Math.max(CleMax, operation.getCle());
            }
        }
        o.setCle(CleMax);

        for (Compte compte : lesComptes) {
            if (compte.getCle() == o.getCleCompte()) {
                compte.addOperation(o.getIntitule(),o.getMontant(),o.getDateOperation());
            }
        }
        jsonManager.setData(lesComptes);
        return o;
    }

    @Override
    public Operation delete(Operation obj) {
        return null;
    }

    @Override
    public Operation update(Operation o) {
        LinkedList<Compte> lesComptes = jsonManager.getData();
        for (Compte compte : lesComptes) {
            for (Operation operation : compte.getLesOperations()) {
                if (operation.getCle() == o.getCle()) {
                    operation.setDateOperation(o.getDateOperation());
                    operation.setIntitule(o.getIntitule());
                    operation.setMontant(o.getMontant());
                }
            }
        }
        jsonManager.setData(lesComptes);
        return o;
    }

    @Override
    public void saveAll(LinkedList<Operation> toSave) {
        //@TODO implements
    }

    @Override
    public Operation findById(int cle) {
        return null;
    }

    @Override
    public LinkedList<Operation> findByName(String name) {
        return null;
    }

    @Override
    public LinkedList<Operation> findAll() {
        LinkedList<Operation> operations = new LinkedList<Operation>();
        LinkedList<Compte> lesComptes = jsonManager.getData();
        for (Compte compte : lesComptes){
            operations.addAll(compte.getLesOperations());
        }
        return operations;
    }
}
