package Decorator;

public class TripleChoco extends CoupeGlace {
	private static TripleChoco instance;
	private int prix=3;
	public static synchronized TripleChoco getInstance(){ 
		if (instance == null) 
		instance = new TripleChoco(); 
		return instance; } 
	public int getPrix(){return prix;}
	private TripleChoco(){}
	public TripleChoco(Dessert d) {
		super();
		
	}
}
