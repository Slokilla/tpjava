package clone;

import clone.TypeForme.FormeTete;
import clone.TypeForme.Taille;

public abstract class Monstre {
	
	protected String nom;
	protected Tete tete;
	protected int NbTete;

	
	public Monstre() {
		nom="Monstre par d�faut";
		tete = new Tete(FormeTete.TeteOurs, 2, Taille.Moyenne);
	}
	
	public Monstre(String nom, Tete tete, int nbTete) {
		super();
		this.nom=nom;
		this.tete = tete;
		NbTete = nbTete;
	}

	public Tete getTete() {
		return tete;
	}

	public void setTypeTete(Tete tete) {
		this.tete = tete;
	}

	public int getNbTete() {
		return NbTete;
	}

	public void setNbTete(int nbTete) {
		NbTete = nbTete;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
}
