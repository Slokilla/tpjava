package clone;

import clone.TypeForme.FormeCorps;
import clone.TypeForme.Taille;

public class Corps {
	private FormeCorps formeCorps=FormeCorps.CorpsHumain;;
	private Taille tailleCorps=Taille.Moyenne;
	
	public Corps() {}

	public Corps(FormeCorps formeCorps, Taille tailleCorps) {
		super();
		this.formeCorps = formeCorps;
		this.tailleCorps = tailleCorps;
	}

	public FormeCorps getForme() {
		return formeCorps;
	}

	public void setForme(FormeCorps forme) {
		this.formeCorps = forme;
	}

	public Taille getTaille() {
		return tailleCorps;
	}

	public void setTaille(Taille taille) {
		this.tailleCorps = taille;
	}
}
