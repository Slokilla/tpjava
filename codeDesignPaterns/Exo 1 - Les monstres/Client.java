package clone;

import java.util.LinkedList;

import clone.TypeForme.FormeCorps;
import clone.TypeForme.FormePatte;
import clone.TypeForme.FormeTete;
import clone.TypeForme.Taille;


public class Client {

	public static void main(String[] args) {
		/*
		 * Cr�ation d'un premier monstre par d�faut
		 */
		MonstreAPattes map2=null, map1 = new MonstreAPattes();
		// Ajoutons-lui quelques pattes
		Patte p=new Patte(FormePatte.PatteDeLion, Taille.Grande);
		map1.addPatte(p);
		p=new Patte(FormePatte.PatteDeLion, Taille.Grande);
		map1.addPatte(p);
		p=new Patte(FormePatte.Serre, Taille.Moyenne);
		map1.addPatte(p);
		p=new Patte(FormePatte.Serre, Taille.Moyenne);
		map1.addPatte(p);
		map1.setNom("Monstre original");
		/*
		 * Affichage du monstre original
		 */
		System.out.println("***********************************************************");
		System.out.println("Monstre original");
		afficherMonstreAPattes(map1);
		
		
		/*
		 * Essai de clonage du monstre 1. Map2 doit �tre une recopie de map1. 
		 * Si on modifie map2, map1 ne doit pas �tre impact�.
		 */
			map2 =map1;
			
	

		/*
		 * Modification du deuxi�me monstre
		 */
		map2.setNom("Monstre clon�");
		map2.getTete().setForme(FormeTete.TeteOiseau);
		map2.getCorps().setForme(FormeCorps.CorpsHumain);
		p=new Patte(FormePatte.bras, Taille.Gigantesque);
		map2.addPatte(p);
		
		/*
		 * Affichage des deux monstres et de leurs adresses m�moires
		 */
		System.out.println("***********************************************************");
		System.out.println("Clone");
		afficherMonstreAPattes(map2);
		System.out.println("***********************************************************");
		System.out.println("Monstre original");
		afficherMonstreAPattes(map1);
	}
	
	public static void afficherMonstreAPattes(MonstreAPattes map )
	{
		System.out.println("********************************************************");
		System.out.println("Affichage du monstre map1 "+map);
		System.out.println("Partie Monstre");
		System.out.println("\tNom : "+ map.getNom());
		System.out.println("\tTete - adresse m�moire tete : "+map.getTete());
		System.out.println("\t\tForme de la tete "+ map.getTete().getForme());
		System.out.println("\t\tTaille de la tete "+ map.getTete().getTailleTete());
		System.out.println("\t\tNombre d'yeux"+ map.getTete().getNbYeux());
		System.out.println("Partie Monstre terrestre"); 
		System.out.println("\tCorps - adresse m�moire corps : "+map.getCorps());
		System.out.println("\t\tForme du corps "+ map.getCorps().getForme());
		System.out.println("\t\tTaille du corps "+ map.getCorps().getTaille());
		System.out.println("Partie MonstreAPattes"); 
		System.out.println("\tLes pattes - adresse m�moire lesPattes : "+map.getLesPattes()); 
		LinkedList<Patte> lesPattes = map.getLesPattes();
		if (lesPattes!=null)
		{
			int i=0;
			for (Patte p:lesPattes)
			{
			i++;
			System.out.println("\t\tPatte "+i +"- adresse "+p); 
			System.out.println("\t\t\tForme patte "+ p.getForme()); 
			System.out.println("\t\t\tTaille patte "+ p.getTaillePatte()); 		
			}
		}
		else
		{
			System.out.println("\tPas de pattes"); 
		}
	}

}
