package clone;


import java.util.LinkedList;

import clone.TypeForme.FormePatte;
import clone.TypeForme.Taille;

public class MonstreAPattes extends MonstreTerrestre {
	private LinkedList<Patte> lesPattes=null;
	
	public MonstreAPattes() {
		super();
	}

	public MonstreAPattes(LinkedList<Patte> lesPattes) {
		super();
		this.lesPattes = lesPattes;
	}

	public LinkedList<Patte> getLesPattes() {
		return lesPattes;
	}

	public void setLesPattes(LinkedList<Patte> lesPattes) {
		this.lesPattes = lesPattes;
	}

	public void addPatte(Patte p)
	{
		if (lesPattes==null) lesPattes=new LinkedList<Patte>();
		lesPattes.add(p);
	}
	public void addPatte(FormePatte f, Taille t)
	{
		if (lesPattes==null) lesPattes=new LinkedList<Patte>();
		lesPattes.add(new Patte(f,t));
		
	}
}