package clone;

import clone.TypeForme.FormeCorps;
import clone.TypeForme.Taille;

public abstract class MonstreTerrestre extends Monstre {
	protected Corps corps;

	public MonstreTerrestre() {
		super();
		corps=new Corps(FormeCorps.CorpsLion, Taille.Gigantesque);
	}

	public MonstreTerrestre(String nom, Tete tete, int nbTete, FormeCorps formeCorps, Taille tailleCorps) {
		super(nom, tete, nbTete);
		corps=new Corps(formeCorps, tailleCorps);
	}

	public Corps getCorps() {
		return corps;
	}

	public void setCorps(Corps corps) {
		this.corps = corps;
	}
	
	
}
